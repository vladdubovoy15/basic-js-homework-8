"use strict";

let input = document.getElementById("input");
let span = document.querySelector(".span");
let text = document.querySelector(".text")

input.addEventListener("blur", () => {
    if(input.value > 0){
        text.textContent = "";
        input.style.cssText = "outline: none; color: green;"
        span.innerHTML = `Текущая цена: ${input.value}
        <span class='close' onclick="remove()"></span>`
        span.style.visibility = "visible";
    } else if (input.value < 0){
        span.style.visibility = "hidden";
        input.style.cssText = "border: 3px solid red; color: black;";
        text.textContent = 'Please enter correct price';
    }    
})

function remove(){
    span.style.visibility = "hidden";
    input.value = "";
    input.style.color = "black";
}
